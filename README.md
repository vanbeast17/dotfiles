https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/
https://wiki.archlinux.org/index.php/Dotfiles#Tracking_dotfiles_directly_with_Git

An alternative method without these drawbacks is the "bare repository and alias method" popularized by this Hacker News comment, which just takes three commands to set up: 
$ git init --bare ~/.dotfiles
$ alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
$ config config status.showUntrackedFiles no
