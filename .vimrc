" ------- Plugin Management -------
" Download vim-plug if it doesn't exist
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'vim-syntastic/syntastic'
Plug 'tomlion/vim-solidity'
Plug 'pangloss/vim-javascript'
Plug 'altercation/vim-colors-solarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
call plug#end()

" ------- END Plugin Management -------

" Solarized colors plugin
syntax enable
set background=dark
colorscheme solarized
" end plugin
"
" Airline plugin
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1
" end plugin

filetype plugin indent on

" Relative numbers of lines
set number relativenumber

" Press jk to quit from Insert mode 
inoremap jk <ESC>

" Control+n show NERDTree
map <C-n> :NERDTreeToggle<CR>

" Press 'Leader(\) and rr to reload vimrc
map <leader>rr :source ~/.vimrc
